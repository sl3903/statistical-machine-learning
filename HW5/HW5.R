#Sitao Luan UNI: sl3903
x=seq(0,4,length=401)
theta=1
f=theta*exp(-theta*x)
plot(x,f,type='l',ylim=c(0,1),xlim=c(0.1,4),ylab='density',main='Density of Exp(1) and Exp(2)')
text(c(1,2,4),f[c(101,201,401)],c('x=1','x=2','x=4'))
theta=2
f2=theta*exp(-theta*x)
lines(x,f2,lty=3)
lines(x=c(1,1),y=c(-0.1,f[101]),lty=4)
lines(x=c(2,2),y=c(-0.1,f[201]),lty=4)
lines(x=c(4,4),y=c(-0.1,f[401]),lty=4)
legend('topright',c('theta=1','theta=2'),lty=c(1,3))



s1=sum(rgamma(4,shape = 1,rate = 1))
s2=sum(rgamma(8,shape = 1,rate = 1))
s3=sum(rgamma(16,shape = 1,rate = 1))
s4=sum(rgamma(256,shape = 1,rate = 1))
plot(x,dgamma(x,shape=2+4,rate=0.2+s1),lty=1,col=1,type='l',ylim=c(0,7),ylab='density',main='Posterior Distributions')
lines(x,dgamma(x,shape=2+8,rate=0.2+s2),lty=2,col=2)
lines(x,dgamma(x,shape=2+16,rate=0.2+s3),lty=3,col=3)
lines(x,dgamma(x,shape=2+256,rate=0.2+s4),lty=4,col=4)
legend('topright',c('n=4','n=8','n=16','n=256'),lty=c(1,2,3,4),col =c(1,2,3,4))

